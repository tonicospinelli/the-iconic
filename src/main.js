// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import VueResource from 'vue-resource'
import App from './App'
import router from './router'
import Paginate from 'vuejs-paginate'
import ApiConfig from './ApiConfig'

require('../node_modules/bootstrap-sass/assets/stylesheets/_bootstrap.scss')
require('../node_modules/bootstrap-sass/assets/fonts/bootstrap/glyphicons-halflings-regular.woff')

Vue.config.productionTip = false
Vue.use(VueResource)
Vue.component('paginate', Paginate)

Vue.http.options.root = ApiConfig.baseUrl

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  template: '<App/>',
  components: { App }
})
