import Vue from 'vue'
import Router from 'vue-router'
import CatalogList from '@/pages/catalog/list'
import ProductDetail from '@/pages/product/detail'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      component: CatalogList
    },
    {
      path: '/:sku',
      component: ProductDetail
    }
  ]
})
